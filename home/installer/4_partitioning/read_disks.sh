#!/bin/bash

#B -> MB B/1048576

# paths variables
u_path=$(pwd)
s_dir=$(dirname $0)
path=$u_path/$s_dir
d_path="$u_path/7_run_installer"

# get disk list
disklist=$(lsblk -drno name,type | awk '/disk/' | awk '{print $1}')

for i in $disklist
do
    diskstr+="/dev/${i} "
done

lsblk -Jdbo name,size,pttype $diskstr > $path/disks.json

# get partition list
partlist=$(lsblk -rno name,type | awk '/part/' | awk '{print $1}')

for i in $partlist
do
    disk=$(echo ${i:: -1})
    all_parts=$(grep -c $disk'[0-9]' /proc/partitions)
    if [[ $all_parts != "0" ]]
    then
        partstr+="/dev/${i} "
    else
        echo "Nothing to do!"
    fi
done

echo $partstr
if [[ -z $partstr ]]
then
    echo "No partitions found"
else
    lsblk -Jdbo name,size,fsused,fstype $partstr >  $path/partitions.json
fi
