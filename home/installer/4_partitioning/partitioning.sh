#!/bin/bash

# B -> MB B/1048576

# paths variables
u_path=$(pwd)
s_dir=$(dirname $0)
path=$u_path/$s_dir
d_path="$u_path/7_run_installer"

# create output json
jq -n '{partitions:{all: []}}' > $path/out.json

# read data from innput json
mode=$(jq -r '.parameters | .mode' $path/in.json)
detect_os=$(jq -r '.parameters | .detect_os' $path/in.json)
os_disk=$(jq -r '.parameters | .os_disk' $path/in.json)
parts_count=$(jq '.parameters | .parts | length' $path/in.json)
arr_max=$(( $parts_count-1 ))


if [[ $mode == "simple" ]]
then
    # quick partitioning
    cp $path/in.json $d_path/partitioning1.json
    jq -n --arg main "0" '{status:{main: $main}}' > $path/status.json
else
    # manual partitioning
    # loop through all partitions
    for (( i=0; i<=$arr_max; i++ ))
    do
        # get disk name
        p=$(jq -r ".parameters | .parts[$i]" $path/in.json)
        # sda / vda
        if [[ "$p" == "sd"* ]] || [[ "$p" == "vd"* ]];
        then
            d=$(echo ${p:: -1})
        else
        # e.g. nvme
            d=$(echo ${p:: -2})
        fi
        # get partition number
        pn=$(echo ${p: -1})
        # get partition size
        s=$(jq -r ".parameters | .p_sizes[$i]" $path/in.json)
        # get filesystem
        fs=$(jq -r ".parameters | .filesystems[$i]" $path/in.json)
        # get mountpoint
        mp=$(jq -r ".parameters | .mountpts[$i]" $path/in.json)

        # generate output json
        jq \
        --arg disk "$d" \
        --arg pnumber "$pn" \
        --arg size "$s" \
        --arg filesystem "$fs" \
        --arg mountpoint "$mp" \
        '.partitions.all += [{part: [($disk), ($pnumber), ($size), ($filesystem), ($mountpoint)]}]' \
        $path/out.json > $path/tmp.json && mv $path/tmp.json $path/out.json
    done

    # check status
    jqs=$(echo $?)

    # send status to json
    if [[ $((jqs)) == 0 ]]
    then
        jq -n --arg main $jqs '{status:{main: $main}}' > $path/status.json
        cp $path/in.json $d_path/partitioning1.json
        cp $path/out.json $d_path/partitioning2.json
    fi
fi
