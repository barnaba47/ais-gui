#!/bin/sh

# paths variables
u_path=$(pwd)
s_dir=$(dirname $0)
path=$u_path/$s_dir
d_path="$u_path/7_run_installer"

# check if system is booted in uefi mode
efivars=$(ls /sys/firmware/efi/efivars | head -n 3)
if [ -z "$efivars" ]
then
    uefiStat=1
else
    uefiStat=0
fi

# export status to json file
jq -n --arg uStat "$uefiStat" '{status:{main: $uStat}}' > $path/status.json

