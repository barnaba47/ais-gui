#!/bin/sh

# paths variables
u_path=$(pwd)
s_dir=$(dirname $0)
path=$u_path/$s_dir
d_path="$u_path/7_run_installer"

# get language variable from file
timezone=$(jq -r '.parameters | .timezone' $path/in.json)

ln -sf /usr/share/zoneinfo/$timezone /etc/localtime
tzStat=$(echo $?)

# export values for main install script
jq -n \
--arg tz "$timezone" '{parameters:{timezone: $tz}}' > $path/out.json

if [[ $tzStat == 0 ]]
then
cp $path/out.json $d_path/timezone.json
fi

# export status to json file
jq -n \
--arg main "$tzStat" '{status:{main: $main}}' > $path/status.json
