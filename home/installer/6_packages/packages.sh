#!/bin/sh

# paths variables
u_path=$(pwd)
s_dir=$(dirname $0)
path=$u_path/$s_dir
d_path="$u_path/7_run_installer"

c_pkg_count=$(jq '.parameters | .c_list | length' $path/in.json)
arr_max=$(( $c_pkg_count-1 ))

# get package option
option=$(jq -r '.parameters | .option' $path/in.json)


case $option in

    gnome)
    FILE=$path/gnome
    if [ -f "$FILE" ]; then
        state=0
        cp $path/gnome $d_path/packages
    else
        state=1
    fi
    ;;

    kde)
    FILE=$path/kde
    if [ -f "$FILE" ]; then
        state=0
        cp $path/kde $d_path/packages
    else
        state=1
    fi
    ;;
    
    cli)
    FILE=$path/cli
    if [ -f "$FILE" ]; then
        state=0
        cp $path/cli $d_path/packages
    else
        state=1
    fi
    ;;

    *)
    ;;
esac

if [[ $state == 0 ]]
then
    # get custom boolean
    custom=$(jq -r '.parameters | .custom' $path/in.json)

    if [[ $custom == "true" ]]
    then
        cp $path/custom $d_path/custom_packages
    fi
fi

cp $path/in.json $d_path/packages.json

# export status to json file
jq -n --arg main "$state" '{status:{main: $main}}' > $path/status.json
