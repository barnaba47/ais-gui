#!/bin/sh

# paths variables
u_path=$(pwd)
s_dir=$(dirname $0)
path=$u_path/$s_dir
d_path="$u_path/7_run_installer"

# create output json
jq -n '{custom:{packages: []}}' > $path/out.json

# create file containing valid custom package list
touch $path/tmpc && mv $path/tmpc $path/custom

c_pkg_count=$(jq '.parameters | .c_list | length' $path/in.json)
arr_max=$(( $c_pkg_count-1 ))

# loop through packages array to check if they are present in official repo
for (( i=0; i<=$arr_max; i++ ))
do
    pkg=$(jq -r ".parameters | .c_list[$i]" $path/in.json)
    echo $pkg | sort > $path/pkg
    check="$(comm -23 $path/pkg $path/repo_list)"
    # actual check
    if [[ $check == "" ]]
    then
        # package exists
        echo $pkg >> $path/custom
    else
        jq --arg chck "$check" '.custom.packages += [($chck)]' $path/out.json > $path/tmp.json && mv $path/tmp.json $path/out.json
    fi

done

rm -f $path/pkg

# export status to json file
jq -n --arg main "$main" '{status:{main: $main}}' > $path/status.json

