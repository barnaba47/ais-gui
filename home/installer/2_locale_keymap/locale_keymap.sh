#!/bin/sh

# paths variables
u_path=$(pwd)
s_dir=$(dirname $0)
path=$u_path/$s_dir
d_path="$u_path/7_run_installer"

# get language variable from file
language=$(jq -r '.parameters | .language' $path/in.json)

# cut parsed string to usable format
locale=$(echo "$language" | cut -d ' ' -f 1)
keymap=$(echo "$language" | cut -c 4-5 )
keymap=$(echo "$keymap" | awk '{print tolower($0)}')

# set installer's keymap

#localectl --no-convert set-x11-keymap $keymap
setxkbmap $keymap
x11kmp=$(echo $?)

# get main script status
if [[ $((x11kmp)) == 0 ]]
then
    main=0;
    # export values for main install script
    jq -n \
    --arg lang "$language" \
    --arg kmp "$keymap" \
    '{parameters:{language: $lang, keymap: $kmp}}' > $path/out.json
    cp $path/out.json $d_path/locale_keymap.json
else
    main=1;
fi


# export status to json file
jq -n --arg main "$main" '{status:{main: $main}}' > $path/status.json
