#!/bin/sh

# paths variables
u_path=$(pwd)
s_dir=$(dirname $0)
path=$u_path/$s_dir
d_path="$u_path/7_run_installer"

# hostname
hostname=$(jq -r '.parameters | .hostname' $path/in.json)

# get user's credentials
username=$(jq -r '.parameters | .username' $path/in.json)
password=$(jq -r '.parameters | .password' $path/in.json)

if  [[ $hostname == "" ]] || [[ $username == "" ]] || [[ $password == "" ]]
then
    main=1
else
    main=0
    cp $path/in.json $d_path/host_user.json
fi

# export status to json file
jq -n --arg main "$main" '{status:{main: $main}}' > $path/status.json

# export repo list for next step
pacman -Ssq | sort > $u_path/6_packages/repo_list
