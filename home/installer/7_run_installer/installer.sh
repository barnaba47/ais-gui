#!/bin/sh

# jq -r --arg x "$i"  '.partitions.all | .[0] | .part[$x|tonumber]' out.json

# print all output to sdtout
set -x

# exit on any error
#set -e

# paths variables
u_path=$(pwd)
s_dir=$(dirname $0)
path=$u_path/$s_dir
d_path="$u_path/7_run_installer"

# json variables
language1=$(jq -r '.parameters | .language' $path/locale_keymap.json)
language2=$(echo "$language1" | cut -d ' ' -f 1)
keymap=$(jq -r '.parameters | .keymap' $path/locale_keymap.json)

timezone=$(jq -r '.parameters | .timezone' $path/timezone.json)

hostname=$(jq -r '.parameters | .hostname' $path/host_user.json)
username=$(jq -r '.parameters | .username' $path/host_user.json)
password=$(jq -r '.parameters | .password' $path/host_user.json)

p_mode=$(jq -r '.parameters | .mode' $path/partitioning1.json)
detect_os=$(jq -r '.parameters | .detect_os' $path/partitioning1.json)
os_disk=$(jq -r '.parameters | .os_disk' $path/partitioning1.json)

option=$(jq -r '.parameters | .option' $path/packages.json)

# error and status handle
err="false"
err_stat()
{
    if [[ $? != "0" ]]
    then
        echo "Error!"
        jq -n --arg status "0" '{status:{main: $status}}' > $path/status.json
        err="true"
    fi
}

err_stat_strict()
{
    if [[ $? != "0" ]]
    then
        echo "Installation failed!"
        jq -n --arg status "1" '{status:{main: $status}}' > $path/status.json
        jq -n --arg status "Installation failed" '{status:{main: $status}}' > $path/progress.json
        exit
    fi
}

# PARTITIONING
jq -n --arg status "Partitioning" '{status:{main: $status}}' > $path/progress.json

# SIMPLE PARTITIONING
simple_partitioning()
{
sfdisk --delete /dev/$os_disk
# prepare disk
parted /dev/$os_disk --script mklabel gpt
parted /dev/$os_disk --script mkpart primary fat32 1 512M p
parted /dev/$os_disk --script set 1 esp on p
parted /dev/$os_disk --script mkpart primary linux-swap 512M 4608M p
parted /dev/$os_disk --script set 2 swap on p
parted /dev/$os_disk --script mkpart primary linux-swap 4608M 100% p

# format partitions
mkfs.vfat -F 32 /dev/${os_disk}1
mkswap -f /dev/${os_disk}2
mkfs.ext4 -F /dev/${os_disk}3

# mount partitions
mount /dev/${os_disk}3 /mnt
mkdir -p /mnt/boot/efi
mount /dev/${os_disk}1 /mnt/boot/efi
swapon /dev/${os_disk}2
}

# MANUAL PARTITIONING
manual_partitioning()
{
parts_count=$(jq '.parameters | .parts | length' $path/partitioning1.json)
arr_max=$(( $parts_count-1 ))

dtd_count=$(jq '.parameters | .wipe_disks | length' $path/partitioning1.json)
dtd=$(jq -r '.parameters | .wipe_disks | .[0] ' $path/partitioning1.json)
dtd_max=$(( $dtd_count-1 ))

ptd_count=$(jq '.parameters | .wipe_parts | length' $path/partitioning1.json)
ptd=$(jq -r '.parameters | .wipe_parts | .[0] ' $path/partitioning1.json)
ptd_max=$(( $ptd_count-1 ))

# umount all drives
#umount -a

# deleting selected partitions
if [[ $ptd != "null" ]]
then
    for (( i=0; i<=$ptd_max; i++ ))
    do
        part=$(jq -r --arg i $i '.parameters | .wipe_parts | .[$i|tonumber]' $path/partitioning1.json)
        p_nr=$(echo ${part: -1})
        disk=$(echo ${part:: -1})

        all_parts=$(grep -c $disk'[0-9]' /proc/partitions)
        max_parts=$(( $all_parts-1 ))

        if [[ $i -lt $max_parts  ]]
        then
            (echo d; echo $p_nr; echo w;) | fdisk /dev/$disk
        elif [[ $i -lt $ptd_max ]]
        then
            (echo d; echo $p_nr; echo w;) | fdisk /dev/$disk
        else
            (echo d; echo w;) | fdisk /dev/$disk
        fi
    done
fi

# deleting all partitions from selected disks
if [[ $dtw != "null" ]]
then
    for (( i=0; i<=$dtd_max; i++ ))
    do
        disk=$(jq -r --arg i $i '.parameters | .wipe_disks | .[$i|tonumber]' $path/partitioning1.json)
        sfdisk --delete /dev/$disk
    done
fi

# creating partitions and formatting loop

for (( i=0; i<=$arr_max; i++ ))
do
    disk=$(jq -r --arg p "$i"  '.partitions.all | .[$p|tonumber] | .part[0]' $path/partitioning2.json)
    p=$(jq -r --arg p "$i"  '.partitions.all | .[$p|tonumber] | .part[1]' $path/partitioning2.json)
    size=$(jq -r --arg p "$i"  '.partitions.all | .[$p|tonumber] | .part[2]' $path/partitioning2.json)
    fs=$(jq -r --arg p "$i"  '.partitions.all | .[$p|tonumber] | .part[3]' $path/partitioning2.json)
    mpt=$(jq -r --arg p "$i"  '.partitions.all | .[$p|tonumber] | .part[4]' $path/partitioning2.json)

    # create partition table if disk is empty
    gptcheck=$(partprobe -s /dev/$disk | grep "gpt")
    sleep 1
    all_parts=$(grep -c $disk'[0-9]' /proc/partitions)

    if [[ -z "$gptcheck" ]] && [[ $all_parts -eq 0 ]]
    then
        (echo g; echo w;) | fdisk /dev/$disk
    else
        echo "GPT already present"
    fi

    case $fs in

    fat32)
        if [[ $i -eq 0 ]] && [[ $all_parts -eq 0 ]]
        then
            (echo n; echo ; echo ; echo "+${size}M"; echo t; echo uefi; echo w) | fdisk /dev/$disk
        else
            (echo n; echo $p; echo ; echo "+${size}M"; echo t; echo $p; echo uefi; echo w) | fdisk /dev/$disk
        fi
        mkfs.vfat -F 32 /dev/$disk$p
        ;;

    ext4)
        if [[ $i -eq 0 ]] && [[ $all_parts -eq 0 ]]
        then
            (echo n; echo ; echo ; echo "+${size}M"; echo t; echo linux; echo w) | fdisk /dev/$disk
        else
            (echo n; echo $p; echo ; echo "+${size}M"; echo t; echo $p; echo linux; echo w) | fdisk /dev/$disk
        fi
        mkfs.ext4 -F /dev/$disk$p
        ;;

    swap)
        if [[ $i -eq 0 ]] && [[ $all_parts -eq 0 ]]
        then
            (echo n; echo ; echo ; echo "+${size}M"; echo t; echo swap; echo w) | fdisk /dev/$disk
        else
            (echo n; echo $p; echo ; echo "+${size}M"; echo t; echo $p; echo swap; echo w) | fdisk /dev/$disk
        fi
        mkswap -f /dev/$disk$p
        ;;
    esac

    # / should be mounted first!
    if [[ $mpt == '/' ]]
    then
        mount /dev/$disk$p /mnt
    fi
done

# mounting partitions loop
for (( i=0; i<=$arr_max; i++ ))
do
    disk=$(jq -r --arg p "$i"  '.partitions.all | .[$p|tonumber] | .part[0]' $path/partitioning2.json)
    p=$(jq -r --arg p "$i"  '.partitions.all | .[$p|tonumber] | .part[1]' $path/partitioning2.json)
    mpt=$(jq -r --arg p "$i"  '.partitions.all | .[$p|tonumber] | .part[4]' $path/partitioning2.json)


    case $mpt in

    /)
        echo "Already mounted!"
        ;;

    /boot)
        mkdir -p /mnt/boot/efi
        mount /dev/$disk$p /mnt/boot/efi
        ;;

    swap)
        swapon /dev/$disk$p
        ;;

    null)
        # nothing to do!
        ;;

    *)
        mkdir -p /mnt$mpt
        mount /dev/$disk$p
        ;;
    esac
done
}

if [[ $p_mode == "simple" ]]
then
    simple_partitioning
    err_stat_strict
else
    manual_partitioning
    err_stat_strict
fi

# downloading base system
jq -n --arg status "Installing base system" '{status:{main: $status}}' > $path/progress.json

pacman -Sy
pacstrap /mnt linux linux-firmware base base-devel
err_stat_strict

# generate filesystem tab
genfstab -U /mnt >> /mnt/etc/fstab
err_stat

# set timezone and enable ntp
tzone()
{
    arch-chroot /mnt ln -sf /usr/share/zoneinfo/$timezone /etc/localtime

}

tzone
    arch-chroot /mnt hwclock --systohc --utc
    arch-chroot /mnt timedatectl set-ntp true
err_stat


# set language and keymap

lang()
{
    mv /mnt/etc/locale.gen /mnt/etc/locale.gen.bak
    echo $language1 > /mnt/etc/locale.gen
    mv /mnt/etc/locale.conf /mnt/etc/locale.conf.bak
    echo LANG='"'$language2'"' > /mnt/etc/locale.conf
    arch-chroot /mnt locale-gen
    arch-chroot /mnt localectl set-keymap --no-convert $keymap
    arch-chroot /mnt localectl --no-convert set-x11-keymap $keymap
}
lang
#err_stat

# set hostname
echo "$hostname" > /mnt/etc/hostname
err_stat_strict

# add user
usr()
{
    arch-chroot /mnt useradd -m -G wheel -s /bin/bash $username
    echo -e "$password\n$password" | arch-chroot /mnt passwd $username
    sed -i '/%wheel ALL=(ALL:ALL) ALL/s/^#//g' /mnt/etc/sudoers
}
usr
err_stat_strict


# pacman configuration
pmconf()
{
    cp /etc/pacman.conf /mnt/etc/pacman.conf
    sed -i '33s/#//' /mnt/etc/pacman.conf
    sed -i '93s/#//' /mnt/etc/pacman.conf
    sed -i '94s/#//' /mnt/etc/pacman.conf
}

pmconf
err_stat_strict

# install bootloader related packages
jq -n --arg status "Installing bootloader" '{status:{main: $status}}' > $path/progress.json

bloader()
{
    arch-chroot /mnt pacman -Sy --noconfirm grub efibootmgr os-prober

    # generate initramfs
    arch-chroot /mnt mkinitcpio -p linux

    # install grub
    arch-chroot /mnt grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=ArchLinux

    # detect other os's
    if [[ $detect_os == "true" ]]
    then
    arch-chroot /mnt os-prober
    fi

    # generate grub config
    arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
    arch-chroot /mnt grub-mkconfig -o /boot/efi/EFI/ArchLinux/grub.cfg
}

bloader
err_stat_strict

# install packages
pkgs()
{
    jq -n --arg status "Installing packages" '{status:{main: $status}}' > $path/progress.json

    arch-chroot /mnt pacman -S --noconfirm networkmanager modemmanager usb_modeswitch xdg-user-dirs

    arch-chroot /mnt pacman -S --noconfirm - < $path/packages
}
pkgs
err_stat_strict

# install custom packages
cpkgs()
{
    jq -n --arg status "Installing custom packages" '{status:{main: $status}}' > $path/progress.json

    c_packages=$path/custom_packages

    if test -f "$c_packages"
    then
    arch-chroot /mnt pacman -S --noconfirm - < $path/custom_packages
    fi
}
cpkgs
err_stat

# enable services
eservices()
{
    arch-chroot /mnt systemctl enable systemd-networkd
    arch-chroot /mnt systemctl enable systemd-resolved
    arch-chroot /mnt systemctl enable NetworkManager
    arch-chroot /mnt systemctl enable ModemManager

    if [[ $option == gnome ]]
    then
        arch-chroot /mnt systemctl enable gdm

    elif [[ $option == kde ]]
    then
        arch-chroot /mnt systemctl enable sddm
    fi
}

eservices
err_stat_strict

fin_status=$(jq -r '.parameters | .main' $path/status.json)

if [[ $err == "false" ]]
then
    jq -n --arg status "Installation finished!" '{status:{main: $status}}' > $path/progress.json
else
    #jq -n --arg status "Installation finished, but minor errors ocurred!" '{status:{main: $status}}' > $path/progress.json
    jq -n --arg status "Installation finished!" '{status:{main: $status}}' > $path/progress.json
fi
