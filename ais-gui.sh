#!/bin/bash

mount -o remount,size=2G /run/archiso/cowspace

yes | pacman -Sy

yes | pacman -S xorg-server xorg-xinit xorg-xsetroot xsettingsd bspwm sxhkd polybar feh qt6-base thunar xfce4-terminal file-roller picom ttf-roboto jq

cp -Rf ~/ais-gui/home/. ~/

chmod +x ~/installer/ais

chmod +x ~/.config/bspwm/bspwmrc ~/.config/sxhkd/sxhkdrc

tar xvzf ~/.icons/BreezeX-Dark.tar.gz -C ~/.icons/

startx &


